//
//  CameraViewController.h
//  Ribbit
//
//  Created by Evan Hurst on 12/8/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end
