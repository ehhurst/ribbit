//
//  AppDelegate.h
//  Ribbit
//
//  Created by Evan Hurst on 11/16/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
