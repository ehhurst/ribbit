//
//  FriendsViewController.h
//  Ribbit
//
//  Created by Evan Hurst on 12/9/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface FriendsViewController : UITableViewController

@property (nonatomic, strong) PFRelation *friendsRelation;
@property (nonatomic, strong) NSArray *friends;

@end
