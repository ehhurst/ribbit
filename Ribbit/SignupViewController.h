//
//  SignupViewController.h
//  Ribbit
//
//  Created by Evan Hurst on 12/8/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

- (IBAction)signup:(id)sender;

@end
