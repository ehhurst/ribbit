//
//  EditFriendsViewController.h
//  Ribbit
//
//  Created by Evan Hurst on 12/9/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface EditFriendsViewController : UITableViewController

@property (nonatomic, strong) NSArray *allUsers;
@property (nonatomic, strong) PFUser *currentUser;
@property (nonatomic, strong) NSMutableArray *friends;

- (BOOL) isFriend:(PFUser *)user;

@end
