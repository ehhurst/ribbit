//
//  InboxViewController.h
//  Ribbit
//
//  Created by Evan Hurst on 12/1/13.
//  Copyright (c) 2013 Evan Hurst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxViewController : UITableViewController

- (IBAction)logOut:(id)sender;

@end
